// 0 path
// 1 wall
// 2 snake
// 3 apple
var map = [
	[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

var points = 0;
var timeout = 400;
var defaultDirection = '>';
var snakeBody = [{ y: 5, x: 8 }, { y: 5, x: 7 }, { y: 5, x: 6 }, { y: 5, x: 5 }, { y: 5, x: 4 }];

document.addEventListener("DOMContentLoaded", documentReady);

function addSnakeToMap() {
	for (var i = 0; i < snakeBody.length; i++) {
		var snakePart = snakeBody[i];
		map[snakePart.y][snakePart.x] = 2;
	}
}

function removeSnakeFromMap() {
	for (var i = 0; i < map.length; i++) {
		for (var j = 0; j < map[i].length; j++) {
			if (map[i][j] == 2) {
				map[i][j] = 0;
			}
		}
	}
}

function nextPostionSnakeHead(snakeHead, direction) {
	var snakeHeadOldPosition = { x: snakeHead.x, y: snakeHead.y };

	switch (direction) {
		case '>':
			snakeHead.x++;
			break;
		case '<':
			snakeHead.x--;
			break;
		case '^':
			snakeHead.y--;
			break;
		case "down":
			snakeHead.y++;
			break;
	}
	return snakeHeadOldPosition;
}

function moveSnakePart(snakePart, previousPart) {
	var snakePartOldPosition = { x: snakePart.x, y: snakePart.y };

	snakePart.x = previousPart.x;
	snakePart.y = previousPart.y;

	return snakePartOldPosition;
}

function addNextPart(lastPosition) {
	snakeBody.push(lastPosition);
}

function initMap(map) {
	var mapContainer = document.getElementById("map");
	mapContainer.innerHTML = '';
	addSnakeToMap();

	for (var i = 0; i < map.length; i++) {
		var div = document.createElement("div");

		for (var j = 0; j < map[i].length; j++) {
			var span = document.createElement("span");

			if (map[i][j] == 0)
				span.setAttribute("class", "path");
			else if (map[i][j] == 1)
				span.setAttribute("class", "wall");
			else if (map[i][j] == 2)
				span.setAttribute("class", "snake");
			else if (map[i][j] == 3)
				span.setAttribute("class", "apple");

			div.appendChild(span);
		}
		mapContainer.appendChild(div);
	}
}

function moveProgram() {
	removeSnakeFromMap();

	//Takes old position of snake's head and changes snake's head position
	var previousPart = nextPostionSnakeHead(snakeBody[0], defaultDirection);

	if (snakeBody[0].x == snakeBody[1].x && snakeBody[0].y == snakeBody[1].y) {
		alert("You're dead because snake eats itself!\n" + messagePoints(points));
		endButton();
		return false;
	}

	//moves snake's body
	for (var i = 1; i < snakeBody.length; i++) {
		previousPart = moveSnakePart(snakeBody[i], previousPart);
	}

	if (isMoveValid()) {
		initMap(map);
		return true;
	}

	//enters here when there is apple or wall 
	else {
		try {
			var snakePosition = snakeBody[0];
			
			if (map[snakePosition.y][snakePosition.x] == 3) {
				addNextPart(previousPart);
				addSnakeToMap()
				initApple();

				points += 10;
				if (timeout > 20)
					timeout -= 50;

				return true;
			}
			else {
				alert("You're dead!\n" + messagePoints(points));
			}
		} catch (error) {
			alert("You're dead!\n" + messagePoints(points));
		}
		endButton();
		return false;
	}
}

//checks if move will be made in scope of map's territorya
function isMoveValid() {
	var snakePosition = snakeBody[0];
	if (snakePosition.y > map.length - 1 || snakePosition.x > map[snakePosition.y].length - 1) {
		return false;
	}

	return map[snakePosition.y][snakePosition.x] == 0;
}

//reads user's input
function uniKeyCode(event) {
	var key = event.keyCode;
	var char = 0;

	if (key == 37) {
		char = '<';
	}
	else if (key == 38) {
		char = '^';
	}
	else if (key == 39) {
		char = '>';
	}
	else if (key == 40) {
		char = "down";
	}

	if (char != 0) {
		defaultDirection = char;
	}
}

function initApple() {
	var x = Math.floor((Math.random() * map[0].length));
	var y = Math.floor((Math.random() * map.length));
	var applePosition = { x: x, y: y };

	if (map[applePosition.y][applePosition.x] == 0) {
		map[applePosition.y][applePosition.x] = 3;
		initMap(map);
	}
	else {
		initApple();
	}
}

function messagePoints(points) {
	if (points == 0 || points <= 30)
		return "Your points: " + points + "\n Too Bad! Try again :)";
	else
		return "Your points: " + points + "\n Good! Try again :)";
}

//creates button to reload the game
function endButton() {
	var btn = document.createElement("button");
	btn.textContent = "PLAY AGAIN";
	btn.style.fontSize = "15px";
	btn.style.width = "100px";
	btn.style.height = "50px";
	btn.style.backgroundColor = "#d83a3a";
	var div = document.getElementById("endGame");
	div.appendChild(btn);

	var startBtn = document.getElementById("startBtn");
	startBtn.style.display = "none";
	btn.onclick = reloadGame;
}

function reloadGame() {
	location.reload();
}

function startGame() {
	var btn = document.getElementById("startBtn");
	btn.style.display = "none";

	document.addEventListener("keydown", uniKeyCode);

	var haveNextMove = moveProgram();
	if (haveNextMove) {
		setTimeout(startGame, timeout);
	}
}

function documentReady() {
	initMap(map);
	initApple();
};